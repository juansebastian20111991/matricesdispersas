package matricesdispersas;

import javax.swing.JOptionPane;
import matricesdispersas.NodoF1;

public class MDlistaF2 {
    private NodoF1 cabeza;

    public MDlistaF2(int nf, int nc) {
        cabeza = new NodoF1(nf, nc, 0);
        cabeza.setLigaFila(cabeza);
        cabeza.setLigaCol(cabeza);
    }

    public NodoF1 getCab() {
        return cabeza;
    }

    public int getNFilas() {
        return cabeza.getFila();
    }

    public int getNColumnas() {
        return cabeza.getCol();
    }

    public int getCdatos() {
        int cont = 0;
        NodoF1 p = cabeza.getLigaFila();
        while (p != cabeza) {
            cont++;
            p = p.getLigaFila();
        }
        return cont;
    }

    public int getFila(int i) {
        if (i <= cabeza.getFila()) {
            NodoF1 p = cabeza.getLigaFila();
            while (p != cabeza) {
                if (i == p.getFila())
                    return p.getFila();
            }
        }
        return Integer.MIN_VALUE;
    }

    public int getColumna(int i) {
        if (i <= cabeza.getCol()) {
            NodoF1 p = cabeza.getLigaCol();
            while (p != cabeza) {
                if (i == p.getCol())
                    return p.getCol();
            }
        }
        return Integer.MIN_VALUE;
    }

    public float getDato(int f, int c) {
        if (f <= cabeza.getFila() && c <= cabeza.getCol()) {
            NodoF1 p = cabeza.getLigaFila();
            while (p != cabeza) {
                if (f == p.getFila() && c == p.getCol())
                    return p.getFila();
            }
            return 0;
        }
        return Float.MIN_VALUE;
    }

    public void setDato(int f, int c, float dato) {

    }

    public void mostrar() {
        String muestra = "";
        NodoF1 p = cabeza.getLigaFila();
        while (p != cabeza) {
            muestra = muestra + p.getDato();
            p = p.getLigaFila();
        }
        JOptionPane.showMessageDialog(null, muestra, "Datos diferentes de cero en la matriz",
                JOptionPane.PLAIN_MESSAGE);
    }

    public void ingresarDatos() {

        int f, c;
        float d;
        String respuesta;

        respuesta = JOptionPane.showInputDialog("Desea ingresar datos?, s/n");

        while (respuesta.equals("s")) {

            f = Integer.parseInt(JOptionPane.showInputDialog("Ingrese fila"));
            c = Integer.parseInt(JOptionPane.showInputDialog("Ingrese columna"));
            d = Float.parseFloat(JOptionPane.showInputDialog("Ingrese dato"));

            this.almacenarDato(f, c, d);
            respuesta = JOptionPane.showInputDialog("Desea ingresar datos?, s/n");
        }
    }

    public void almacenarDato(int f, int c, float d) {
        NodoF1 p = cabeza.getLigaFila(), q, antF = cabeza, antC = cabeza;
        while (p != cabeza && p.getFila() < f) {
            antF = p;
            p = p.getLigaFila();
        }
        while (p != cabeza && p.getFila() == f && p.getCol() < c) {
            antF = p;
            p = p.getLigaFila();
        }
        p = cabeza.getLigaCol();
        while (p != cabeza && p.getCol() < c) {
            antC = p;
            p = p.getLigaCol();
        }
        while (p != cabeza && p.getCol() == c && p.getFila() < f) {
            antC = p;
            p = p.getLigaCol();
        }
        if (p != cabeza && p.getCol() == c && p.getFila() == f)
            JOptionPane.showMessageDialog(null, "El Dato " + p.getDato() + " ya existe en la posicion",
                    "Ya existe un dato", JOptionPane.INFORMATION_MESSAGE);
        else {
            NodoF1 x = new NodoF1(f, c, d);
            x.setLigaFila(antF.getLigaFila());
            antF.setLigaFila(x);
            x.setLigaCol(p);
            antC.setLigaCol(x);
        }
    }

    public void insertarDato(int f, int c, float d) {
        if (d != 0) {
            NodoF1 p = cabeza.getLigaFila(), q, antF = cabeza, antC = cabeza;
            while (p != cabeza && p.getFila() < f) {
                antF = p;
                p = p.getLigaFila();
            }
            while (p != cabeza && p.getFila() == f && p.getCol() < c) {
                antF = p;
                p = p.getLigaFila();
            }
            p = cabeza.getLigaCol();
            while (p != cabeza && p.getCol() < c) {
                antC = p;
                p = p.getLigaCol();
            }
            while (p != cabeza && p.getCol() == c && p.getFila() < f) {
                antC = p;
                p = p.getLigaCol();
            }
            if (p != cabeza && p.getCol() == c && p.getFila() == f) {
                if (p.getDato() + d != 0)
                    p.setDato(p.getDato() + d);
                else {
                    antF.setLigaFila(p.getLigaFila());
                    antC.setLigaCol(p.getLigaCol());
                }
            } else {
                NodoF1 x = new NodoF1(f, c, d);
                x.setLigaFila(antF.getLigaFila());
                antF.setLigaFila(x);
                x.setLigaCol(p);
                antC.setLigaCol(x);
            }
        }
    }

    public float[] sumarFilas() {
        NodoF1 p = cabeza.getLigaFila();
        float[] sum = new float[cabeza.getCol()];
        while (p != cabeza) {
            sum[p.getCol()] = sum[p.getCol()] + p.getDato();
            p = p.getLigaFila();
        }
        return sum;
    }

    public void promedioCol() {
        float sum[] = sumarFilas();
        for (int k = 1; k <= cabeza.getCol(); k++)
            JOptionPane.showMessageDialog(null, sum[k - 1] / cabeza.getFila(), "Promedio columna " + k,
                    JOptionPane.PLAIN_MESSAGE);
    }

    public float mayormdLista() {
        float mayor = Float.MIN_VALUE;

        return mayor;
    }

    public MDlistaF2 sumar(MDlistaF2 b) {
        if (b != null) {
            if (this.getNFilas() == b.getNFilas() && this.getNColumnas() == b.getNColumnas()) {
                MDlistaF2 res = new MDlistaF2(b.getNFilas(), b.getNColumnas());
                for (int i = 0; i <= this.getNFilas(); i++) {
                    for (int j = 0; j <= this.getNColumnas(); j++)
                        res.insertarDato(i, j, this.getDato(i, j) + b.getDato(i, j));
                }
                return res;
            }
        }
        return null;
    }

    public MDlistaF2 multiplicar(MDlistaF2 b) {
        return b;
    }

    public boolean comparar(MDlistaF2 b) {
        return false;
    }

    public MDlistaF2 copia() {
        MDlistaF2 r = new MDlistaF2(cabeza.getFila(), cabeza.getCol());
        NodoF1 p = cabeza.getLigaFila();
        while (p != cabeza) {
            r.insertarDato(p.getFila(), p.getCol(), p.getDato());
            p = p.getLigaFila();
        }
        return r;
    }
}