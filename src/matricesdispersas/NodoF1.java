/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package matricesdispersas;

/**
 *
 * @author sala311
 */
public class NodoF1 {
    
    int fila,col;
    float dato;
    NodoF1 ligaFila,ligaCol,liga;

    public NodoF1(int fila, int col, float dato) {
        this.fila = fila;
        this.col = col;
        this.dato = dato;
        liga = null;
        ligaFila = null;
        ligaCol = null;
    }

    public int getFila() {
        return fila;
    }

    public void setFila(int fila) {
        this.fila = fila;
    }

    public int getCol() {
        return col;
    }

    public void setCol(int col) {
        this.col = col;
    }

    public float getDato() {
        return dato;
    }

    public void setDato(float dato) {
        this.dato = dato;
    }

    public NodoF1 getLigaFila() {
        return ligaFila;
    }

    public void setLigaFila(NodoF1 ligaFila) {
        this.ligaFila = ligaFila;
    }

    public NodoF1 getLigaCol() {
        return ligaCol;
    }

    public void setLigaCol(NodoF1 ligaCol) {
        this.ligaCol = ligaCol;
    }

    public NodoF1 getLiga() {
        return liga;
    }

    public void setLiga(NodoF1 liga) {
        this.liga = liga;
    }   
    
    
    
}
