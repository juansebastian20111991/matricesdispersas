/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package matricesdispersas;

import javax.swing.*;

/**
 *
 * @author sala311
 */
public class MDTripletas {

    int nFilas, nColumnas, nTripletas;
    double[][] ListaTrip;

    public MDTripletas(int nf, int nc, int cd) {
        nFilas = nf;
        nColumnas = nc;
        nTripletas = cd + 1;

        ListaTrip = new double[nTripletas][3];
        ListaTrip[0][0] = nf;
        ListaTrip[0][1] = nc;
        ListaTrip[0][2] = cd;
    }

    public void Mostrar() {

        String salida = "";
        int t = 1;
        for (int i = 0; i < nFilas; i++) {
            for (int j = 0; j < nColumnas; j++) {

                if (t <= ListaTrip[0][2] && ListaTrip[t][0] == i && ListaTrip[t][1] == j) {
                    salida += ListaTrip[t][2] + " ";
                    t++;
                } else {
                    salida += "0.0 ";
                }
            }

            salida += "\n";
        }

        JOptionPane.showMessageDialog(null, salida);
    }

    public void AlmacenarTrip(int f, int c, double d) {

        int k = 1, j;
        while (k < ListaTrip[0][2] + 1 && ListaTrip[k][0] < f && ListaTrip[k][2] != 0) {
            k++;
        }

        while (k < ListaTrip[0][2] + 1 && ListaTrip[k][0] <= f && ListaTrip[k][1] < c && ListaTrip[k][2] != 0) {
            k++;
        }

        if (k < ListaTrip[0][2] + 1 && ListaTrip[k][0] == f && ListaTrip[k][1] == c && ListaTrip[k][2] != 0) {
            JOptionPane.showMessageDialog(null, "No se puede almacenar el dato porque ya existe el termino");
        } else {

            for (j = (int) ListaTrip[0][2]; j > k; j--) {
                ListaTrip[j][0] = ListaTrip[j - 1][0];
                ListaTrip[j][1] = ListaTrip[j - 1][1];
                ListaTrip[j][2] = ListaTrip[j - 1][2];
            }

            ListaTrip[k][0] = f;
            ListaTrip[k][1] = c;
            ListaTrip[k][2] = d;
        }
    }

    // Metodo para ingresar los datos de la matriz
    public void IngresarDatos() {

        int f, c;
        double d;

        // for(int k = 0; k < ListaTrip[0][2]; k++)
        // for(int k = 1; k < nTripletas; k++)
        for (int k = 0; k < nTripletas - 1; k++) {
            f = Integer.parseInt(JOptionPane.showInputDialog("Ingrese fila"));
            c = Integer.parseInt(JOptionPane.showInputDialog("Ingrese columna"));

            if (f >= 0 && f < ListaTrip[0][0] && c >= 0 && c < ListaTrip[0][1]) {
                d = Double.parseDouble(JOptionPane.showInputDialog("Ingrese dato"));
                AlmacenarTrip(f, c, d);
            } else {
                JOptionPane.showMessageDialog(null, "Rango no valido para la matriz");
                k--;
            }
        }
    }

    public void redimensionar() {
        int k;
        double aux[][] = new double[nTripletas + 1][3];
        for (k = 0; k <= ListaTrip[0][2]; k++) {
            aux[k][0] = ListaTrip[k][0];
            aux[k][1] = ListaTrip[k][1];
            aux[k][2] = ListaTrip[k][2];
        }
        ListaTrip = aux;
    }

    public void InsertarTrip(int f, int c, double d) {
        if (d != 0) {
            int k = 1, j;
            while (k <= ListaTrip[0][2] && ListaTrip[k][0] < f && ListaTrip[k][2] != 0) {
                k++;
            }

            while (k <= ListaTrip[0][2] && ListaTrip[k][0] <= f && ListaTrip[k][1] < c && ListaTrip[k][2] != 0) {
                k++;
            }

            if (k <= ListaTrip[0][2] && ListaTrip[k][0] == f && ListaTrip[k][1] == c) {
                if (ListaTrip[k][2] + d != 0)
                    ListaTrip[k][2] = ListaTrip[k][2] + d;
                else {
                    for (j = k; j < ListaTrip[0][2]; j++) {
                        ListaTrip[j][0] = ListaTrip[j + 1][0];
                        ListaTrip[j][1] = ListaTrip[j + 1][1];
                        ListaTrip[j][2] = ListaTrip[j + 1][2];
                    }
                    ListaTrip[0][2] = ListaTrip[0][2] - 1;
                }
            } else {
                if (ListaTrip[0][2] + 1 == nTripletas) {
                    redimensionar();
                }

                for (j = (int) ListaTrip[0][2] - 1; j >= k; j--) {
                    ListaTrip[j + 1][0] = ListaTrip[j][0];
                    ListaTrip[j + 1][1] = ListaTrip[j][1];
                    ListaTrip[j + 1][2] = ListaTrip[j][2];
                }

                ListaTrip[k][0] = f;
                ListaTrip[k][1] = c;
                ListaTrip[k][2] = d;
                ListaTrip[0][2] = ListaTrip[0][2] + 1;
                nTripletas = (int)ListaTrip[0][2] + 1;
            }
        }
    }

}
