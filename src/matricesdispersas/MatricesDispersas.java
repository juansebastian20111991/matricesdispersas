/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package matricesdispersas;

import javax.swing.JOptionPane;

/**
 *
 * @author sala311
 */
public class MatricesDispersas {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        int opcion;

        MDlistaF1 mdF1 = null;
        MDlistaF2 mdF2 = null;
        MDTripletas mdTp = null;
        int nf, nc, cd;

        String menu = "**MENÚ** \n 1.Mostrar Tipo 1 \n 2. Mostrat tipo 2\n 3.Ingresar tipo 1\n 4.Ingresar tipo 2\n 5.Sumar lista forma 1 y forma 2 y almacenar en tripleta\n  0.Salir  ";

        do {
            opcion = Integer.parseInt(JOptionPane.showInputDialog(menu));

            switch (opcion) {
            case 1:
                if (mdF1 != null) {
                    mdF1.mostrar();
                } else {
                    JOptionPane.showMessageDialog(null, "esta vacio.");
                }
                break;

            case 2:

                if (mdF2 != null) {
                    mdF2.mostrar();
                } else {
                    JOptionPane.showMessageDialog(null, "esta vacio.");
                }
                break;
            case 3:
                nf = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el numero de filas de la matriz"));
                nc = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el numero de columnas de la matriz"));

                mdF1 = new MDlistaF1(nf, nc);
                mdF1.ingresarDatos();

                break;

            case 4:
                nf = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el numero de filas de la matriz"));
                nc = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el numero de columnas de la matriz"));

                mdF2 = new MDlistaF2(nf, nc);
                mdF2.ingresarDatos();

                break;
            case 5:
                /*
                 * nf = Integer.parseInt(JOptionPane.
                 * showInputDialog("Ingrese el numero de filas de la matriz lista forma 1")); nc
                 * = Integer.parseInt(JOptionPane.
                 * showInputDialog("Ingrese el numero de columnas de la matriz lista forma 1"));
                 * mdF1 = new MDlistaF1(nf, nc); mdF1.ingresarDatos();
                 * 
                 * nf = Integer.parseInt(JOptionPane.
                 * showInputDialog("Ingrese el numero de filas de la matriz lista forma 2")); nc
                 * = Integer.parseInt(JOptionPane.
                 * showInputDialog("Ingrese el numero de columnas de la matriz lista forma 2"));
                 * mdF2 = new MDlistaF2(nf, nc); mdF2.ingresarDatos();
                 */

                if (mdF1 != null && mdF2 != null) {
                    mdTp = mdF1.sumar(mdF2);
                    if (mdTp != null) {
                        mdTp.Mostrar();
                    } else {
                        JOptionPane.showMessageDialog(null, "Debe ingresar una matriz en lista forma 1 y forma 2");
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Debe ingresar una matriz en lista forma 1 y forma 2");
                }

                break;
            default:
                JOptionPane.showMessageDialog(null, "Opción no válida.");

            }

        } while (opcion != 0);

        /*
         * MDTripletas mdt; int nf, nc, cd;
         * 
         * nf = Integer.parseInt(JOptionPane.
         * showInputDialog("Ingrese el numero de filas de la matriz")); nc =
         * Integer.parseInt(JOptionPane.
         * showInputDialog("Ingrese el numero de columnas de la matriz")); cd =
         * Integer.parseInt(JOptionPane.
         * showInputDialog("Ingrese la cantidad de datos diferentes de 0"));
         * 
         * mdt = new MDTripletas(nf, nc, cd); mdt.IngresarDatos(); mdt.Mostrar();
         */

    }

}
