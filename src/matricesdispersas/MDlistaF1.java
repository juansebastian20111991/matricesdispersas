package matricesdispersas;

import javax.swing.JOptionPane;

public class MDlistaF1 {

    NodoF1 cab;

    public MDlistaF1(int fila, int column) {

        cab = crearCabezas(fila, column);
    }

    public int getNFilas() {
        return cab.getFila();
    }

    public int getNColumnas() {
        return cab.getCol();
    }

    public NodoF1 crearCabezas(int fila, int column) {

        int n, k;
        NodoF1 p, x, ultimoNodoLista;

        p = new NodoF1(fila, column, 0);
        p.setLigaFila(p);
        p.setLigaCol(p);
        ultimoNodoLista = p;

        if (fila > column) {
            n = fila;
        } else {
            n = column;
        }

        for (k = 0; k < n; k++) {
            x = new NodoF1(k, k, 0);
            x.setLigaCol(x);
            x.setLigaFila(x);
            ultimoNodoLista.setLiga(x);
            ultimoNodoLista = x;
        }

        ultimoNodoLista.setLiga(p);

        return p;
    }

    public void mostrar() {

        NodoF1 p = cab.getLiga(), q;
        String salida = "";
        int k, j;

        /* while (p.getLiga() != cab) { */
        for (k = 0; k < cab.getFila(); k++) {
            q = p.getLigaFila();
            for (j = 0; j < cab.getCol(); j++) {
                if (q.getFila() == k && q.getCol() == j) {
                    salida += q.getDato() + " ";
                    q = q.getLigaFila();
                } else {
                    salida += "0.0 ";
                }
            }
            salida += "\n";
            p = p.getLiga();
        }
        /*
         * q = p.getLigaFila(); while (q!=p) { salida+=q.getDato()+" ";
         * q=q.getLigaFila(); } }
         */
        JOptionPane.showMessageDialog(null, "Datos \n" + salida);
    }

    public void insertarDato(int f, int c, float d) {

        NodoF1 p, q, anteriorFila, anteriorColumna, x;
        p = cab.getLiga();
        while (p != cab && p.getFila() < f) {
            p = p.getLiga();
        }
        anteriorFila = p;
        q = p.getLigaFila();
        while (q != p && q.getCol() < c) {
            q = q.getLiga();
        }

        p = cab.getLiga();

        while (p != cab && p.getCol() < c) {
            p = p.getLiga();
        }
        anteriorColumna = p;
        q = p.getLigaCol();
        while (q != p && q.getFila() < f) {
            q = q.getLiga();
        }

        if (q != p && q.getFila() == f && q.getCol() == c) {
            if ((q.getDato() + d) != 0) {
                q.setDato((q.getDato() + d));
            } else {
                anteriorFila.setLigaFila(q.getLigaFila());
                anteriorColumna.setLigaCol(q.getLigaCol());
                // delete(q) se libera el nodo
            }
        } else {
            x = new NodoF1(f, c, d);
            x.setLigaFila(anteriorFila.getLigaFila());
            anteriorFila.setLigaFila(x);
            x.setLigaCol(anteriorColumna.getLigaCol());
            anteriorColumna.setLigaCol(x);
        }
    }

    public void almacenarDato(int f, int c, float d) {

        NodoF1 p, q, anteriorFila, anteriorColumna, x;
        p = cab.getLiga();
        while (p != cab && p.getFila() < f) {
            p = p.getLiga();
        }

        q = p.getLigaFila();
        anteriorFila = q;
        while (q != p && q.getCol() < c) {
            anteriorFila = q;
            q = q.getLigaFila();
        }

        p = cab.getLiga();

        while (p != cab && p.getCol() < c) {
            p = p.getLiga();
        }

        q = p.getLigaCol();
        anteriorColumna = q;
        while (q != p && q.getFila() < f) {
            anteriorColumna = q;
            q = q.getLigaCol();
        }

        if (q != p && q.getFila() == f && q.getCol() == c) {
            JOptionPane.showInputDialog(null, "Ya existe dato en esa posición");
        } else {
            x = new NodoF1(f, c, d);
            x.setLigaFila(anteriorFila.getLigaFila());
            anteriorFila.setLigaFila(x);
            x.setLigaCol(anteriorColumna.getLigaCol());
            anteriorColumna.setLigaCol(x);
        }
    }

    public void ingresarDatos() {

        int f, c;
        float d;
        String respuesta;

        respuesta = JOptionPane.showInputDialog("Desea ingresar datos?, s/n");

        while (respuesta.equals("s")) {

            f = Integer.parseInt(JOptionPane.showInputDialog("Ingrese fila"));
            c = Integer.parseInt(JOptionPane.showInputDialog("Ingrese columna"));
            if (f >= 0 && f < cab.getFila() && c >= 0 && c < cab.getCol()) {
                d = Float.parseFloat(JOptionPane.showInputDialog("Ingrese dato"));
                this.almacenarDato(f, c, d);
            } else {
                JOptionPane.showMessageDialog(null, "La posición no es válida");
            }
            respuesta = JOptionPane.showInputDialog("Desea ingresar datos?, s/n");
        }
    }

    public MDTripletas sumar(MDlistaF2 b) {
        MDTripletas tp = null;
        NodoF1 pF1 = cab.getLiga(), pF2 = b.getCab(), qF1, qF2;
        int k, j, cd = 0;

        if (b != null) {
            if (b.getNFilas() == this.getNFilas() && b.getNColumnas() == this.getNColumnas()) {
                // Saber cuantos valores diferentes de 0 existen
                for (k = 0; k < this.getNFilas(); k++) {
                    qF1 = pF1.getLigaFila();
                    qF2 = pF2.getLigaFila();

                    for (j = 0; j < b.getNColumnas(); j++) {
                        if ((qF1.getFila() == k && qF1.getCol() == j) || (qF2.getFila() == k && qF2.getCol() == j)) {
                            cd++;
                        }
                    }
                    pF1 = pF1.getLiga();
                    pF2 = qF2;
                }

                // Inicializar MDTripleta
                tp = new MDTripletas(this.getNFilas(), this.getNColumnas(), 0);

                pF1 = cab.getLiga();
                pF2 = b.getCab();

                for (k = 0; k < this.getNFilas(); k++) {
                    qF1 = pF1.getLigaFila();
                    qF2 = pF2.getLigaFila();

                    for (j = 0; j < b.getNColumnas(); j++) {
                        if (qF1.getFila() == k && qF1.getCol() == j) {
                            tp.InsertarTrip(k, j, qF1.getDato());
                            pF1 = pF1.getLiga();
                        }
                        
                        if (qF2.getFila() == k && qF2.getCol() == j) {
                            tp.InsertarTrip(k, j, qF2.getDato());
                            pF2 = qF2;
                        }
                    }
                }
            }
        }

        return tp;
    }

}
